find_package(PkgConfig)

PKG_CHECK_MODULES(PC_GR_GNSSPP gnuradio-gnsspp)

FIND_PATH(
    GR_GNSSPP_INCLUDE_DIRS
    NAMES gnuradio/gnsspp/api.h
    HINTS $ENV{GNSSPP_DIR}/include
        ${PC_GNSSPP_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /var/empty/local/include
          /var/empty/include
)

FIND_LIBRARY(
    GR_GNSSPP_LIBRARIES
    NAMES gnuradio-gnsspp
    HINTS $ENV{GNSSPP_DIR}/lib
        ${PC_GNSSPP_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /var/empty/local/lib
          /var/empty/local/lib64
          /var/empty/lib
          /var/empty/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/gnuradio-gnssppTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GR_GNSSPP DEFAULT_MSG GR_GNSSPP_LIBRARIES GR_GNSSPP_INCLUDE_DIRS)
MARK_AS_ADVANCED(GR_GNSSPP_LIBRARIES GR_GNSSPP_INCLUDE_DIRS)
