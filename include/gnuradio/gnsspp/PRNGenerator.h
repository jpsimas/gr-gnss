/* -*- c++ -*- */
/*
 * Copyright 2024 Joao P O Simas.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_GNSSPP_PRNGENERATOR_H
#define INCLUDED_GNSSPP_PRNGENERATOR_H

#include <gnuradio/gnsspp/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace gnsspp {

    /*!
     * \brief <+description of block+>
     * \ingroup gnsspp
     *
     */
    class GNSSPP_API PRNGenerator : virtual public gr::sync_block
    {
     public:
      typedef std::shared_ptr<PRNGenerator> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of gnsspp::PRNGenerator.
       *
       * To avoid accidental use of raw pointers, gnsspp::PRNGenerator's
       * constructor is in a private implementation
       * class. gnsspp::PRNGenerator::make is the public interface for
       * creating new instances.
       */
      static sptr make(uint16_t outMask, int period);
    };

  } // namespace gnsspp
} // namespace gr

#endif /* INCLUDED_GNSSPP_PRNGENERATOR_H */
