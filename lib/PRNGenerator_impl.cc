/* -*- c++ -*- */
/*
 * Copyright 2024 Joao P O Simas.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/io_signature.h>
#include "PRNGenerator_impl.h"

namespace gr {
  namespace gnsspp {

    namespace PRNGeneratorDetail {
      template<int N_BITS = 10>
      uint16_t booleanInnerProduct(const uint16_t x, const uint16_t y) {
	uint16_t masked = x & y;
	uint16_t product = 0;
	for(auto i = 0; i < N_BITS; i++) {
	  product ^= (masked & 1);
	  masked >>= 1;
	}
	return product;
      }
    } // namespace PRNGeneratorDetail
    
    using output_type = uint16_t;
    PRNGenerator::sptr
    PRNGenerator::make(uint16_t outMask2, int period)
    {
      return gnuradio::make_block_sptr<PRNGenerator_impl>(outMask2, period);
    }


    /*
     * The private constructor
     */
    PRNGenerator_impl::PRNGenerator_impl(uint16_t outMask2, int period)
      : state({initialState[0], initialState[1]}),
	outputMask({0b1000000000, outMask2}),
	period(period),
	count(0),
	gr::sync_block("PRNGenerator",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(1 /* min outputs */, 1 /*max outputs */, sizeof(output_type)))
    {}

    /*
     * Our virtual destructor.
     */
    PRNGenerator_impl::~PRNGenerator_impl()
    {
    }

    int
    PRNGenerator_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      auto out = static_cast<output_type*>(output_items[0]);

      // Do <+signal processing+>

      constexpr uint16_t seqPeriod = (1 << 10) - 1;
      constexpr uint16_t seqSize = seqPeriod;

      for(auto i = 0; i < noutput_items; i++) {
	for(auto j = 0; j < 2; j++) {
	  // lfsr update
	  uint16_t feedback = PRNGeneratorDetail::booleanInnerProduct(state[j], feedbackMask[j]);

          state[j] = (state[j] << 1) & ((1 << 10) - 1);
	  state[j] |= feedback;

	  // compute output
	  out[i] ^= PRNGeneratorDetail::booleanInnerProduct(state[j], outputMask[j]);
	}
	
	count++;
	if((count % period) == 0) {
	  reset();
	  count = 0;
	}
      }
      
      
      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

    void
    PRNGenerator_impl::reset() {
      state[0] = initialState[0];
      state[1] = initialState[1];
    }
    
  } /* namespace gnsspp */
} /* namespace gr */
