/* -*- c++ -*- */
/*
 * Copyright 2024 Joao P O Simas.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_GNSSPP_PRNGENERATOR_IMPL_H
#define INCLUDED_GNSSPP_PRNGENERATOR_IMPL_H

#include <gnuradio/gnsspp/PRNGenerator.h>

namespace gr {
  namespace gnsspp {

    class PRNGenerator_impl : public PRNGenerator
    {
     private:
      // Nothing to declare in this block.
      uint16_t state[2];
      const uint16_t outputMask[2];
      static constexpr uint16_t feedbackMask[2] = {0b1000000100, 0b1110100110};

      static constexpr uint16_t initialState[2] = {(1 << 10) - 1, (1 << 10) - 1};
      
      const int period;

      int count;
     public:
      PRNGenerator_impl(uint16_t outMask2, int period);
      ~PRNGenerator_impl();

      // Where all the action really happens
      int work(
              int noutput_items,
              gr_vector_const_void_star &input_items,
              gr_vector_void_star &output_items
      );

      void reset();
    };

  } // namespace gnsspp
} // namespace gr

#endif /* INCLUDED_GNSSPP_PRNGENERATOR_IMPL_H */
