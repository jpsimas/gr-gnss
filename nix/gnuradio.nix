{ pkgs ? import <nixpkgs> {} }:

pkgs.gnuradio.override {
  extraPythonPackages = [
    # add python libs to use inside GNU Radio here
  ];
  extraPackages = [
    (pkgs.callPackage ./gr-gnsspp.nix {})
    # add other GNU Radio block libraries here
    (pkgs.callPackage ../../gr-adappp/nix/gr-adappp.nix {})
  ];
  # extraSoapySdrPackages = [
  #   pkgs.soapyhackrf
  # ];
}

