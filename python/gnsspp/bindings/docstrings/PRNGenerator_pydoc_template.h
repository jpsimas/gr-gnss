/*
 * Copyright 2024 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr, gnsspp, __VA_ARGS__)
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */



 static const char *__doc_gr_gnsspp_PRNGenerator = R"doc()doc";


 static const char *__doc_gr_gnsspp_PRNGenerator_PRNGenerator = R"doc()doc";


 static const char *__doc_gr_gnsspp_PRNGenerator_make = R"doc()doc";

  
